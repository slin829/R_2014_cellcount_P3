HCstats <- function(treatment, cell.type){ 
  IHC<-function(t){
    myData <- read.csv("cellsur.csv", header=TRUE)
    data <- myData[,-4]
    data <- data[, -c(5:12)]
    if(cell.type =="IHC"){
      data <- data[,1:6]
    }
    else if(cell.type=="OHC"){
      data <- data[,-c(7:9)]
      data <- data[, -c(6,8)]
    }
    
    control <- subset(data, data$Treatment %in% t)
    cochlea <- names(table(droplevels(control$Cochlea.Number)))
    subData <- vector()
    for(i in cochlea){
      input <- subset(control[,6], control$Cochlea.Number %in% i)
      subData <- cbind(subData, input)
    }
    colnames(subData) <- cochlea
    IHC.sur <- colMeans(subData, na.rm=TRUE)
    IHC.sur <- as.data.frame(IHC.sur)
    IHC.sur <- cbind(rep(t),IHC.sur)
    IHC.sur
  }
  
  result <- data.frame()
  for(i in treatment){
    input1 <- IHC(i)
    result <- rbind(result,input1)
  }
  result
  colnames(result) <- c("Treatment", "HC.sur")
  
  kw <- kruskal.test(HC.sur ~ Treatment, data=result)
  print(kw)
  posthoc <- pairwise.wilcox.test(result$HC.sur, result$Treatment, p.adj="BH", exact=F)
  posthoc.data<-data.frame(posthoc$p.value)
  significance <- posthoc.data
  significance[significance < 0.001] <- "***"
  significance[significance < 0.01] <- "**"
  significance[significance < 0.05] <- "*"
  significance[significance > 0.05] <- "-"
  significance
  
}
