# Version 7 --- plots cell survival
indcochlgm.g <- function(cochlea, cell.type = "all"){
  ## read data and convert into data.frame
  myData <- read.csv(paste(Dir, '/cellsur.csv', sep=""), header = TRUE)
  Data <- data.frame(myData)  
  
  # plotting
  x <- seq(5,100, by= 5)
  table <- subset(Data, Data[,2] %in% cochlea)
  axis <- c(0:100)
  if(cell.type == "IHC"){
    plot(x,table[,15], 
         type="o", 
         pch=20,
         col="green",
         xlim=c(0,100), 
         ylim=range(axis), 
         xlab="", ylab="")
    par(fg="black")
  }else if(cell.type == "OHC1"){
    plot(x,table[,16], 
         type="o", 
         pch=1,
         col="green",
         lty = 3,
         xlim=c(0,100), 
         ylim=range(axis), 
         xlab="", ylab="")
  }else if(cell.type == "OHC2"){
    plot(x,table[,17], 
         type="o", 
         pch=4,
         col="green",
         lty = 4,
         xlim=c(0,100), 
         ylim=range(axis), 
         xlab="", ylab="")
    par(fg="black")
  }else if(cell.type == "OHC3"){
    plot(x,table[,18], 
         type="o",
         pch=2,
         col="green",
         lty = 2,
         xlim=c(0,100), 
         ylim=range(axis), 
         xlab="", ylab="")
  }else if(cell.type == "OHC"){
    plot(x,table[,19], 
         type="o", 
         pch=20,
         col="green",
         xlim=c(0,100), 
         ylim=range(axis), 
         xlab="", ylab="")
    par(fg="black")
  }else if(cell.type == "dOHC"){
    plot(x,table[,16], 
         type="o", 
         pch=1,
         col="green",
         lty = 3,
         xlim=c(0,100), 
         ylim=range(axis), 
         xlab="", ylab="")
    par(new=TRUE)
    plot(x,table[,17], 
         type="o", 
         pch=4,
         col="green",
         lty = 4,
         xlim=c(0,100), 
         ylim=range(axis), 
         xlab="", ylab="")
    par(new=TRUE)
    plot(x,table[,18], 
         type="o",
         pch=2,
         col="green",
         lty = 2,
         xlim=c(0,100), 
         ylim=range(axis), 
         xlab="", ylab="")
    par(fg="black")
  }else if(cell.type == "THC"){
    plot(x,table[,20], 
         type="o", 
         pch=20,
         col="green",
         xlim=c(0,100), 
         ylim=range(axis), 
         xlab="", ylab="")
    par(fg="black")
  }else if(cell.type == "both"){
    plot(x,table[,15], 
         type="o", 
         pch=20,
         col="green",
         xlim=c(0,100), 
         ylim=range(axis), 
         xlab="", ylab="")
    par(new=TRUE)
    plot(x,table[,19], 
         type="o", 
         pch=20,
         col="green",         
         xlim=c(0,100), 
         ylim=range(axis), 
         xlab="", ylab="") 
    par(fg="black")
  }else if(cell.type == "all"){
    plot(x,table[,15], 
         type="o", 
         pch=20,
         col="green",
         xlim=c(0,100), 
         ylim=range(axis), 
         xlab="", ylab="")
    par(new=TRUE)
    plot(x,table[,19], 
         type="o", 
         pch=20,
         col="green",
         xlim=c(0,100), 
         ylim=range(axis),  
         xlab="", ylab="")
    par(new=TRUE)
    plot(x,table[,20], 
         type="o", 
         pch=20,
         col="green",
         xlim=c(0,100), 
         ylim=range(axis),  
         xlab="", ylab="")
    par(fg="black")
    par(new=TRUE)
  }
  #legend=c("IHC","OHC", "OHC1", "OHC2", "OHC3", "Total HC")
  #legend("bottomleft", legend , lty= c(1,1,3,4,2,1), pch=c(20,20,1,4,2,20),col=c("blue","red","red","red","red","green"))
#  title(main = substitute(paste("Hair cell survival: ", k), list(k=cochlea)),
        #xlab="% Distance from apex", 
        #ylab="% Hair cell survival")
  axis(1, at = seq(10,100, by = 10))
  title(main = ,
      xlab="% Distance from apex", 
      ylab="% Hair cell survival")
}