sink("stats.sig.doc")
atpneo.par <- nonpar(c("control", "neo","atp", "atpneo"),"OHC")
atpneo_l.par <- nonpar(c("control", "neo","atp", "atpneo_l"),"OHC")
atpneo_h.par <- nonpar(c("control", "neo","atp", "atpneo_h"),"OHC")
atpgsneo.par <- nonpar(c("control", "neo","atpgs", "atpgsneo"),"OHC")
utpneo.par <- nonpar(c("control", "neo","utp", "utpneo"),"OHC")
utpgsneo.par <- nonpar(c("control", "neo","utpgs", "utpgsneo"),"OHC")
p2r.par <- nonpar(c("atpgs", "atpgsneo","utpgs", "utpgsneo"),"OHC")
aneo.par <- nonpar(c("control", "neo", "a", "aneo"),"OHC")
adacneo.par <- nonpar(c("control", "neo", "adac", "adacneo"),"OHC")
p1r.par <- nonpar(c("control", "neo", "aneo", "adacneo"),"OHC")

print("ATPneo")
atpneo.par
print("ATPneo_l")
atpneo_l.par
print("ATPneo_h")
atpneo_h.par
print("ATPgSneo")
atpgsneo.par
print("UTPneo")
utpneo.par
print("UTPgSneo")
utpgsneo.par
print("P2Rs")
p2r.par
print("Aneo")
aneo.par
print("ADACneo")
adacneo.par
print("P1R")
p1r.par
sink()